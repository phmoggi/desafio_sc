
DC := docker-compose

start:
	$(DC) start

init: start
	$(DC) exec the-db createdb -U postgres -O postgres baseb
	$(DC) exec the-db createdb -U postgres -O postgres basec

up:
	$(DC) up -d

test:
	$(DC) run the-api python3 manage.py test

stop:
	$(DC) stop
