
from django.urls import path, include
from rest_framework import routers


router = routers.DefaultRouter()
# router.register('user', UserViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    # path('api/auth/', include('rest_framework.urls')),
]
