
# Definições e Especificações

Este projeto define o armazenamento, processamento e disponibilização dos
dados dos consumidores, de arquitetura à definição de serviços.

<!--  -->

## Banco de dados e suas entidades

O sistema utiliza-se de apenas três banco de dados (que poderiam estar
separados fisicamente, porém aqui mantidos no mesmo cluster por praticidade).

### As bases

Os banco de dados são definidos como segue:

- **Base A:** Dados do consumidor que podem ser mutáveis pelo mesmo;
- **Base B:** Dados validados sobre o consumidor, que só podem ser mudados por
 pessoas credenciadas;
- **Base C:** Dados históricos do consumidor.

### Entidades

Algumas entidades, aqui especificadas, podem conter uma leve simplificação das
utilizadas no projeto real, visto que, por exemplo, a entidade Usuário (do
Django) possui mais atributos (como Grupos e Permissões) que não serão
utilizadas.

As entidades aqui definidas não possuirão sinais de pontuação na implementação.

A entidade *Usuário* (definida pelo Django), mantida na base A, possui:

- **ID** primary_key integer;
- **Username** varchar(150), unique, not_null;
- **Primeiro Nome** varchar(30);
- **Último Nome** varchar(150);
- **Senha** varchar(128) not_null;
- **Email** varchar(254);
- **is_staff** boolean;
- **is_active** boolean;
- **is_superuser** boolean;
- **last_login** timestamp;

A entidade *Consumidor*, mantida na base A, possui:

- **ID** primary_key integer;
- **UUID** char(32); unique;
- **CPF** numeric(11,0);
- **Data Nascimento** date;
- **Endereço** varchar(255);
- **Fonte de Renda** varchar(50);

A entidade *Dívidas*, mantida na base A, possui:

- **ID** primary_key integer;
- **CPF** numeric(11,0);
- **Divida** varchar(50);

A entidade *Consumidor_Validado*, mantida na base B, possui:

- **ID** primary_key integer;
- **UUID** char(32); unique;
- **CPF** numeric(11,0);
- **Data Nascimento** date;
- **Endereço** varchar(255);
- **Fonte de Renda** varchar(50);

A entidade *Bens*, mantida na base B, possui:

- **ID** primary_key integer;
- **UUID** char(32);
- **Ben** varchar(50);

A entidade *Consultas*, mantida na base C, possui:

- **ID** primary_key integer;
- **CPF** numeric(11,0);
- **Compania** varchar(50);

A entidade *Movimentação_Financeira*, mantida na base C, possui:

- **ID** primary_key integer;
- **CPF** numeric(11,0);
- **Movimentação** varchar(50);

A entidade *Histórico_Compras*, mantida na base C, possui:

- **ID** primary_key integer;
- **CPF** numeric(11,0);
- **Compra** varchar(50);

<!--  -->

## Modelos a nível de aplicação e suas Permissões

Por utilizar o framework Django, há uma ligeira facilidade de se definir uma
entidade identica a outra em outro banco, e explicitamente identifica-las em
código (Ver link 1 no [Anexo](#Anexo)).

As permissões utilizadas neste sistema são baseadas nas Entidades em sí, e não
como definida pela entidade *Permissões* do Django, facilitando o entendimento
e implementação em um primeiro momento, mas pecando conceitualmente em
abrangencia.

A utilização da entidade Usuário, definida pelo Django, possibilita a definir
visão de cliente (consumidor) e visão de administrador do sistema.
A visão de *Administrador* será provida através da flag is_staff na entidade
Usuário (podendo adicionar o modulo de Admin do Django).

<!--  -->

## Rotas/Endpoints e retornos da API

Todos os endpoint possuem as operações GET/HEAD/OPTIONS por padrão, sendo
explicitamente definido que não, quando aplicável.

- **/users/**: Necessita permissão de Administrador
  - **/users/login/**
  - **/users/logout/**
- **/customers/**
  - **/customers/uuid/**
  - **/customers/login/**: Necessita cadastro de Consumidor
  - **/customers/logout/**: Necessita cadastro de Consumidor
  - **/customers/goods/**
  - **/customers/debts/**
  - **/customers/consults/**
  - **/customers/transactions/credit/**
  - **/customers/transactions/financial/**

<!--  -->

## ANEXO

Link 1 - [Multiple databases on Django](
https://docs.djangoproject.com/en/2.2/topics/db/multi-db/#multiple-databases)
