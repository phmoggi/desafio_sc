FROM python:3.7

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/requirements.txt

RUN pip install -r requirements.txt

COPY . /usr/src/app

ENV APP_DIR /usr/src/app

WORKDIR $APP_DIR

EXPOSE 8080

CMD ["python3", "manage.py", "runserver", "0:8080"]
