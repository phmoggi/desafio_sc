
# Desafio Sistema de Gestão de Consumidor

Sistema desenvolvido com intuito de armazenar, processar e disponibilizar
informações sobre consumidores.

## Dependências

O sistema foi desenvolvido com Python (3.7) e Django (2.2) e projetado para
utilizar Docker Compose, tendo sua execução descrita em [Execução](#Execução).

As tecnologias são:

- Docker Machine (0.16.1)
- Docker Compose (1.24.1)
- Docker Community (18.09.6)
- Python (3.7)
- Django (2.2)
- Celery (4.3.0)
- Redis (5.0)
- RabbitMQ (3.7)
- PostgreSQL (10.9)

## Especificação e Definições

As definições de rotas, retornos, entidades, modelos e outros estão descritas
no arquivo [SPECIFICATION.md](SPECIFICATION.md) e toda parte descritiva sobre o projeto se limita
à este arquivo.

## Execução

```sh
git clone <this> desafio_sc
cd desafio_sc

# Com o Docker Machine funcionando:
make init
```

## Teste

```sh
# Com o Docker Machine funcionando:
make test
```
